let socket = io.connect('http://35.157.80.184:8080');
let user = "Guest";
let builtDiv = "";

document.getElementById("userName").value = user;
socket.on('message', data => {
    buildMessage(data);
});

function buildMessage(msg) {
    var messageDiv = document.getElementById("messages");

    if (msg.user === user) {
        builtDiv += `<div class="my-message">${msg.message}</div>`;
    } else {
        builtDiv += `<div class="message">${msg.user}: ${msg.message}</div>`;
    }

    messageDiv.innerHTML = builtDiv;
}

document.getElementById('submitMsg').addEventListener('click', () => {
    let userName = document.getElementById('userName').value;
    let message = document.getElementById('message').value;

    if (user !== userName) {
        console.log(`User: ${user} updated her/his name to: ${userName}`);
        user = userName;
    }

    sendMessage(user, message);
});

function sendMessage(usr, msg) {
    console.log(`User: ${usr} sending message: ${msg}`);
    let data = {message: msg, user: usr};
    socket.emit('message', data);
}